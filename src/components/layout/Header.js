import React from "react";

class Header extends React.Component{

    render(){
        return (
            <header className="headerStyle">
                <div style={logo}>
                    <img style={logoStyle} src='https://cdn.dribbble.com/users/3267379/screenshots/6098927/e_shop.jpg' alt ="store-logo"/>
                    <div>Fake Store</div>
                </div>
                <div style={accountOptions}>
                    <div className='header-btn' style={login}>
                        Login
                    </div>
                    <div className='header-btn' style={signUp}>
                        Sign up
                    </div>
                </div>
            </header>
        )
    }
}


const logo={
    display: 'flex',
    alignItems: 'center',
    fontSize: '3rem',
    flexWrap : 'wrap'
}

const logoStyle = { 
    width: '4rem',
    height: '3.5rem',
    borderRadius: '1rem',
    margin: '1rem'
}

const accountOptions={
    display: 'flex',
    alignItems: 'center',
    flexWrap : 'wrap',
    gap: '1rem',
    fontSize: '2rem'
}

const login = {
    fontSize : '1.5rem',
    padding: '0.5rem 2rem',
    borderRadius: '1rem'
}

const signUp={
    fontSize : '1.5rem',
    padding: '0.5rem 2rem',
    borderRadius: '1rem'
}


export default Header;