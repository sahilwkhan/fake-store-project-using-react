import React from "react";
import './products.css';

class Products extends React.Component{


    render(){
        const productData = this.props.products;
        // console.log(this.props);

        return (
            <div className="product-container">
                {productData.map((productArray)=>{
                    return(
                        <div key={productArray.id} className="item-container">
                            <img className='productImages' key={productArray.id} src={productArray.image} alt={productArray.title}/>
                            <div className="title">{productArray.title}</div>
                            <div className="price">${productArray.price}</div>
                            <div className="category">Category : {productArray.category}</div>
                            <div className="rating"> Rating : {productArray.rating.count} users rated {productArray.rating.rate} / 5</div>
                        </div>
                    )
                })};
            </div>
        )
    }
}


export default Products;
