import React from 'react'
import './search.css'

class Search extends React.Component{


  render() {
      return (
        <div className='searchBarStyle' >
            <form >
              <input
                type='text'
                name='title'
                placeholder='Search Fake Store'
                className='inputTextStyle'
              />
              <input
                type='submit'
                value='Submit'
                className='inputBarStyle'
              />
            </form>
        </div>
        )
    }
}

export default Search;