import React from 'react';
import './App.css';
import Header from './components/layout/Header';
import Search from './components/search/Search';
import Products from './components/products/Products';
import Loader from './components/loader/Loader';


class App extends React.Component {
  
  constructor(props){
    super(props);
    
    this.API_STATES = {
      LOADING : "loading",
      LOADED : "loaded",
      ERROR : "error",
    }

    this.state = {
      products: [],
      status: this.API_STATES.LOADING,
      errorMessage: "",
    }
  }

  componentDidMount() {

    this.setState({
      status: this.API_STATES.LOADING,
    }, () => {
      fetch('https://fakestoreapi.com/products')
        .then((response) => {
          return response.json();
        })
        .then((productData) => {          
          return this.setState({
            products: productData,
            status: this.API_STATES.LOADED,
          });
        })
        .catch((erorrFetchingProducts) => {
          console.error("Failed to fetch products from API :- ");
          console.error(erorrFetchingProducts);
          this.setState({
            status: this.API_STATES.ERROR,
            errorMessage: "Error occured while loading products from server. Try again after few minutes."
          });
        })
    });

  }


  render(){

    const { products } = this.state;
    // console.log(this.state.products,"<----");
   
    return (

      <div className="App">
        <Header />
        <Search />
      
        <div className='image-container'>
          
          {this.state.status === this.API_STATES.LOADING && <Loader/> }

          {this.state.status === this.API_STATES.ERROR &&
            <div className='error'>
              <h1>{this.state.errorMessage}</h1>
            </div>
          }

          {this.state.status === this.API_STATES.LOADED && products.length  == 0 &&
            <div className='empty-products-error'>
              <h1>Unable to load any products at this moment. Please try later.</h1>
            </div>
          }
          
          {this.state.status === this.API_STATES.LOADED && products.length > 0 &&
             <Products  products={this.state.products}/>
          }
          
        </div>

        
      </div>

    );
  }
}

export default App;
